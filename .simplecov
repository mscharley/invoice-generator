require 'simplecov-bamboo'

SimpleCov.at_exit do
  SimpleCov.result.format!

  if ENV['RAKE_FINAL_TEST'].nil? || ENV['RAKE_FINAL_TEST'].to_i != 0
    SimpleCov.minimum_coverage 90
  end
end

SimpleCov.start 'test_frameworks' do
  coverage_dir 'target/coverage'
  formatter SimpleCov::Formatter::BambooFormatter
  add_filter '/lib/monoxide/invoice/provider/test_provider.rb'
  # Temporary, till we get some coverage going on it
  add_filter '/lib/monoxide/invoice/provider/harvest.rb'
end
