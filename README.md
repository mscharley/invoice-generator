Invoice Generator
=================

Invoice generator that grabs data from various time tracking software and populates a user specified ERB template
with data. Currently supported platforms:

* AtTask
* Harvest

To add a new platform, look at Monoxide::Invoice::Provider::GenericProvider and the other classes in that module for
guidance.
