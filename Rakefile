
desc 'Cleans up any generated folders'
task :clean do
  rm_rf 'target'
  rm_rf 'test/reports'
end

desc 'Runs all code tests to ensure code quality'
task :test => :clean do
  run_tests(
      'bundle exec cane',
      'bundle exec rspec --format documentation',
      'bundle exec cucumber',
  ) or fail('Tests failed or insufficient code coverage')
end

namespace :test do
  task :ci => :clean do
    run_tests(
        'bundle exec cane',
        'bundle exec rspec --format progress --format JUnit --out test/reports/rspec.xml',
        'bundle exec cucumber --profile ci',
    ) or fail('Tests failed or insufficient code coverage')
  end
end

def run_tests(*tests)
  ret = true

  tests.each do |executable|
    ENV['RAKE_FINAL_TEST'] = (tests[-1] == executable ? '1' : '0')
    sh executable do |ok, _|
      ret = false unless ok
    end
  end

  return ret
end
