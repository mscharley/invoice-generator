#!/bin/bash

if [ -z "$BUILD_RUBY" ]; then
  echo "No ruby version defined, please set BUILD_RUBY."
  exit 1
fi

if [ "x$BAMBOO_RESTART" = 'x' ]; then
  export BAMBOO_RESTART=1
  bash --login $0
  exit $?
fi

rvm use $BUILD_RUBY
if [ $? -ne 0 ]; then
  echo "Failed to run RVM to switch to ruby $BUILD_RUBY"
  exit 1
fi

bundle install
rake test:ci

exit $?
