require 'date'
require 'yaml'

require 'highline'
require 'trollop'

require 'monoxide/invoice/provider/generic_provider'

module Monoxide
  module Invoice
    # This class abstracts away user interface between the library and the frontend. Currently only supports CLI via
    # Highline and Trollop, hence the name; however this should be generalised and a GUI version created in the future.
    class Cli
      VERSION = '1.0.0'

      def initialize(menu_provider = nil)
        @highline = menu_provider || HighLine.new
      end

      def parse_options(options)
        @options = options

        if options[:config].is_a? Hash
          @config = options[:config]
        else
          config_file = if options[:config]
                          options[:config]
                        else
                          File.join(Dir.home, '.invoices.yml')
                        end

          unless File.exist? config_file
            Trollop::die "Unable to read configuration file, please create at #{config_file}"
          end

          @config = YAML.load_file(config_file)
        end
      end

      attr_reader :data

      # @param [Hash] options
      # @param [Array] files
      # @return [void]
      def run(options, files)
        if files.empty?
          Trollop::die 'No template file specified'
        end

        parse_options(options)

        files.each do |template|
          unless File.readable? template
            Trollop::die "Unable to read template file: #{template}"
          end

          process_template(template)
        end
      end

      def process_template(template)
        company = @highline.choose {|menu| company_select_menu template, menu}
        output = @highline.ask('Output to: ') { |q| q.default = "#{company}.#{template[-template.reverse.index('.')..-1]}" }

        feedback 'Fetching data...'
        data = fetch_data(@config[company.to_s])
        @data = select_data(data)

        feedback 'Generating invoice...'
        generate(template, output, @config[company.to_s], @data)
      end

      def fetch_data(company)
        provider = Provider::GenericProvider.create_provider(company['type'].to_sym, company)

        # False positive
        #noinspection RubyHashKeysTypesInspection
        Hash[provider.data.sort_by {|key, _| key }.last(10).reverse]
      end

      def select_data(data)
        options = data.keys
        chosen = options.first(2)

        # #timesheet_menu will display a CUI menu to the user to select options from
        until @highline.choose{|menu| timesheet_menu options, chosen, menu} == :done
        end

        data.select {|k, _| chosen.include? k}
      end

      # Locals are available to ERB and used in the templates
      #noinspection RubyUnusedLocalVariable
      def generate(template, output_file, company, data)
        File.write(output_file, ERB.new(File.read(template), nil, '-').result(binding))
      end

      private

      def feedback(string)
        puts(string) unless @options[:quiet]
      end

      def timesheet_menu(options, chosen, menu)
        menu.prompt = 'Please select which timesheets to use:'
        options.sort { |a, b| -(a <=> b) }.each do |k|
          menu.choice(k + (chosen.include?(k) ? ' (*)' : '')) do
            if chosen.include? k
              chosen.delete k
            else
              chosen.push k
            end
          end
        end
        menu.choice(:done)
      end

      def company_select_menu(template, menu)
        menu.prompt = "Select company for #{template}:"
        @config.each do |key, company|
          menu.choice(key, company['name'])
        end
      end
    end
  end
end
