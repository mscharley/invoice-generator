
require 'attask'

module Monoxide
  module Invoice
    module Provider
      # Data provider for AtTask.
      #
      # @see http://www.attask.com/
      class Attask < GenericProvider
        def initialize(config)
          @client = config[:client] ||
              ::Attask.client(config['auth']['subdomain'], config['auth']['username'], config['auth']['password'])
          @data = Hash.new {|hash, key| hash[key] = Hash.new(0) }
        end

        def data
          (not @data.empty?) ? @data : begin
            process_timesheets @client.timesheet \
                .search({:fields => 'startDate,hours:hours,hours:hourType:name,hours:project:name'}, {:status => 'C'}) \
                .sort { |a, b| a['startDate'] <=> b['startDate'] }

            @data
          end
        end

        def sort_timesheets(a, b)
          if a['project'].nil? and b['project'].nil?
            a['hourType']['name'] <=> b['hourType']['name']
          elsif a['project'].nil?
            1
          elsif b['project'].nil?
            -1
          else
            a['project']['name'] <=> b['project']['name']
          end
        end

        private

        def process_timesheets(timesheets)
          timesheets.each do |t|
            t['hours'].select { |h| not h['hourType']['name'].start_with? 'Leave - ' }.
                sort(&method(:sort_timesheets)).
                each do |h|
              if h['project'].nil?
                @data[t['startDate']][h['hourType']['name']] += h['hours']
              else
                @data[t['startDate']][h['project']['name']] += h['hours']
              end
            end
          end
        end
      end
    end
  end
end
