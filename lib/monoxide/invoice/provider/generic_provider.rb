
module Monoxide
  module Invoice
    module Provider
      # Generic provider class. To create your own new provider, simply subclass this and implement the `#data` method.
      # The constructor should take a single parameter, the config hash as entered in the .yml config file for the
      # company of your type.
      class GenericProvider
        def initialize; end

        def self.create_provider(type, config)
          case type
            when :attask
              Attask.new config

            when :harvest
              Harvest.new config

            when :test
              TestProvider.new config

            else
              raise "Invalid type of timesheet provider: #{type}"
          end
        end

        def data
          raise 'Unimplemented'
        end
      end
    end
  end
end

# Require down here so we have GenericProvider defined
require 'monoxide/invoice/provider/attask'
require 'monoxide/invoice/provider/harvest'
require 'monoxide/invoice/provider/test_provider'
