require 'harvested'

module Harvest
  module API
    # Monkey patch harvested with some updates that have been sitting on someone's branch a long time now.
    # @see https://github.com/zmoazeni/harvested/issues/62
    class Time < Base
      def projects(user = nil)
        response = request(:get, credentials, '/daily', :query => of_user_query(user))
        Hash[JSON.parse(response.body)['projects'].map {|project|
          project['tasks'] = Hash[project['tasks'].map {|task|
            [task['id'], task]
          }]
          [project['id'], project]
        }]
      end
    end
  end
end

module Monoxide
  module Invoice
    module Provider
      # Data provider for Harvest
      #
      # @see http://www.getharvest.com/
      class Harvest < GenericProvider
        def initialize(config)
          @client = config[:client] ||
              ::Harvest.hardy_client(
                  config['auth']['subdomain'],
                  config['auth']['username'],
                  config['auth']['password']
              )
          @data = Hash.new {|hash, key| hash[key] = Hash.new(0) }
        end

        def data
          (not @data.empty?) ? @data : begin
            projects = @client.time.projects

            # Grab 10 weeks of data; this matches what will be allowed to show to the user.
            @client.reports.time_by_user(account_user_id, Date.today - 70 - Date.today.wday, Date.today + 1).each do |t|
              project = projects[t['project_id']]
              client = project['client']
              project = project['name']

              @data[(t['spent_at'] - t['spent_at'].wday).to_s]["#{client} - #{project}"] += t['hours']
            end

            @data
          end
        end

        private

        def account_user_id
          (@client.account.who_am_i)['id']
        end
      end
    end
  end
end
