
module Monoxide
  module Invoice
    module Provider
      # Data provider for testing purposes. You can inject static data for it to return via ::generate_data.
      # Excluded from code coverage.
      #noinspection RubyClassVariableUsageInspection
      class TestProvider < GenericProvider
        def initialize(config); end

        def self.generate_data(weeks)
          current_week = Date.today - Date.today.wday - 7
          @@data = {}

          (1..weeks).each do |i|
            @@data[current_week.to_s] = {
              "Test amount" => 5,
              "Test amount 2" => 15,
            }

            current_week = current_week - 7
          end
        end

        def data
          @@data
        end
      end
    end
  end
end
