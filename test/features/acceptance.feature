Feature: Overall acceptance tests

  Scenario: General acceptance test
    Given I am using the command line
    And I have an appropriate template
    And there are 6 weeks of data
    When I select a company
    And I set the output file to "test.yml"
    And I select 3 in the menu
    And run the application
    Then there should be 3 timesheets
    And I should have a file called "test.yml"
    And output file should contain "Test Company"
    And output file should contain "42 Test St"
