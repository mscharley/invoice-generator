Feature: Data Providers
  In order to generate invoices
  As a business owner
  I want to access data from timesheets in various data providers

  Scenario: No changes made in menu
    Given I am using the command line
    And there are 4 weeks of data
    When I fetch data from a provider
    And I ignore the menu
    And check the data
    Then there should be 2 timesheets

  Scenario: Item removed via menu
    Given I am using the command line
    And there are 4 weeks of data
    When I fetch data from a provider
    And I select 1 in the menu
    And check the data
    Then there should be 1 timesheets

  Scenario: Item added via menu
    Given I am using the command line
    And there are 4 weeks of data
    When I fetch data from a provider
    And I select 3, 4 in the menu
    And check the data
    Then there should be 4 timesheets
