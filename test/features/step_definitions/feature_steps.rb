require 'monoxide/invoice/cli'
require 'monoxide/invoice/provider/test_provider'
require 'tmpdir'

#noinspection RubyUnusedLocalVariable
class FakeHighline
  def initialize()
    @options = []
    @answers = []
  end

  def push_answer(answer)
    @answers.push(answer)
  end

  def push_menu_choices(*options)
    @options += options
  end

  def ask(question)
    @answers.shift
  end

  def choose
    @choices = {}
    @index = 1
    yield self

    opt = @options.shift
    if opt.is_a?(Fixnum) && !@choices[opt][:block].nil?
      @choices[opt][:block].call @choices[opt][:key]
    end
    @choices[opt][:key]
  end

  def prompt=(value);end
  def choice(key, value=nil, &block)
    @choices[key] = {:key => key, :block => block}
    @choices[@index] = {:key => key, :block => block}
    @index += 1
  end
end

Given(/^there are (\d+) weeks of data$/) do |count|
  Monoxide::Invoice::Provider::TestProvider.generate_data(count.to_i)
  #noinspection RubyStringKeysInHashInspection
  @company = {
    'type' => 'test',
    'name' => 'Test Company',
    'address' => '42 Test St',
  }
end

Given(/^I am using the command line$/) do
  @menu = FakeHighline.new
  @cli = Monoxide::Invoice::Cli.new(@menu)
end

Given(/^I have an appropriate template$/) do
  @template = File.join(Dir.tmpdir, 'template.yml')
  File.write(@template, <<-EOT)
company: <%= company['name'] %>
address: <%= company['address'] %>
charges:
<% data.each do |date, charges| -%>
  <%= date %>:
<% charges.each do |name, hours| -%>
    <%= name %>: <%= hours %>
<% end -%>
<% end -%>
  EOT
end

When(/^I select a company$/) do
  @menu.push_menu_choices(1)
end

When(/^I set the output file to "(.*?)"$/) do |filename|
  @menu.push_answer File.join(Dir.tmpdir, filename)
end

When(/^run the application$/) do
  #noinspection RubyStringKeysInHashInspection
  @cli.run({
               :config => {'test' => @company},
               :quiet => true,
           }, [@template])
  @data = @cli.data
end

When(/^I fetch data from a provider$/) do
  @data = @cli.fetch_data(@company)
end

When(/^I ignore the menu$/) do
  @menu.push_menu_choices(:done)
end

When(/^I select ((?:\d+)(?:,\s*\d+)*) in the menu$/) do |options|
  options = options.split(',').map{|s| s.strip.to_i}
  @menu.push_menu_choices(*options, :done)
end

When(/^check the data$/) do
  @data = @cli.select_data(@data)
end

Then(/^there should be (\d+) timesheets/) do |count|
  @data.count.should == count.to_i
end

Then(/^I should have a file called "(.*?)"$/) do |filename|
  filename = File.join(Dir.tmpdir, filename)
  File.exists?(filename).should == true
  @output_file = File.read(filename)
end

Then(/^output file should contain "(.*?)"$/) do |substring|
  @output_file.index(substring).should_not == nil
end
