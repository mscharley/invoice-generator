require 'bundler/setup'
require 'simplecov'

# Allow our lib files to be included naturally
$:.push(File.join(Dir.getwd, 'lib'))
