require 'monoxide/invoice/provider/generic_provider'

#noinspection RubyStringKeysInHashInspection
ATTASK_TEST_DATA = [
    {
        'startDate' => '2014-01-19',
        'hours' => [
            {
                'hourType' => {'name' => 'Task Time'},
                'project' => {'name' => 'Test Project'},
                'hours' => 16,
            },
            {
                'hourType' => {'name' => 'Leave - Annual'},
                'project' => nil,
                'hours' => 8,
            },
            {
                'hourType' => {'name' => 'Task Time'},
                'project' => {'name' => 'Test Project'},
                'hours' => 8,
            },
            {
                'hourType' => {'name' => 'Sales'},
                'project' => nil,
                'hours' => 8,
            },
        ],
    },
    {
        'startDate' => '2014-01-12',
        'hours' => [
            {
                'hourType' => {'name' => 'Task Time'},
                'project' => {'name' => 'Test Project'},
                'hours' => 8,
            },
            {
                'hourType' => {'name' => 'Sales'},
                'project' => nil,
                'hours' => 8,
            },
        ],
    },
]

describe Monoxide::Invoice::Provider::Attask do
  def generate_client
    client = double('attask-client')
    client.stub(:timesheet) do
      timesheet = double
      timesheet.stub(:search) do |params, _|
        @search_params = params
        ATTASK_TEST_DATA
      end
      timesheet
    end
    client
  end

  it 'is returned when creating a provider with type :attask' do
    #noinspection RubyStringKeysInHashInspection
    expect(Monoxide::Invoice::Provider::GenericProvider.create_provider(:attask, {
        'auth' => {
            'username' => 'test',
            'password' => 'test',
            'subdomain' => 'test',
        }
    }).class).to eq(Monoxide::Invoice::Provider::Attask)
  end

  describe '#data' do
    before(:each) do
      @client = generate_client
      @provider = Monoxide::Invoice::Provider::GenericProvider.create_provider(:attask, {:client => @client})
    end

    it 'asks for all needed data at once' do
      @provider.data
      expect(@search_params[:fields]).to eq('startDate,hours:hours,hours:hourType:name,hours:project:name')
    end

    it 'ignores time entries that are for "Leave - "' do
      expect(@provider.data['2014-01-19']['Leave - Annual']).to eq(0)
    end

    it 'sorts timesheets chronologically with the oldest items first' do
      expect(@provider.data.keys.first).to eq('2014-01-12')
    end

    it 'returns an appropriate hash based on input from AtTask' do
      expect(@provider.data['2014-01-19']['Test Project']).to eq(24)
    end
  end

  describe '#sort_timesheets' do
    before(:each) do
      @client = generate_client
      @provider = Monoxide::Invoice::Provider::GenericProvider.create_provider(:attask, {:client => @client})
    end

    it 'sorts two project time entries alphabetically by project name' do
      project_1 = 'Test Project'
      project_2 = 'Some Other Project'

      #noinspection RubyStringKeysInHashInspection
      expect(@provider.sort_timesheets(
          {
              'hourType' => {'name' => 'Task Time'},
              'project' => {'name' => project_1},
              'hours' => 16,
          }, {
              'hourType' => {'name' => 'Task Time'},
              'project' => {'name' => project_2},
              'hours' => 16,
          }
      )).to eq(project_1 <=> project_2)
    end

    it 'sorts two non-project time entries alphabetically by hour type name' do
      type_1 = 'Sales'
      type_2 = 'Admin'

      #noinspection RubyStringKeysInHashInspection
      expect(@provider.sort_timesheets(
                 {
                     'hourType' => {'name' => type_1},
                     'project' => nil,
                     'hours' => 16,
                 }, {
                     'hourType' => {'name' => type_2},
                     'project' => nil,
                     'hours' => 16,
                 }
             )).to eq(type_1 <=> type_2)
    end

    it 'sorts project time entries ahead of non-project time entries' do
      #noinspection RubyStringKeysInHashInspection
      expect(@provider.sort_timesheets(
                 {
                     'hourType' => {'name' => 'Task Time'},
                     'project' => {'name' => 'Project'},
                     'hours' => 16,
                 }, {
                     'hourType' => {'name' => 'Admin'},
                     'project' => nil,
                     'hours' => 16,
                 }
             )).to eq(-1)

      #noinspection RubyStringKeysInHashInspection
      expect(@provider.sort_timesheets(
                 {
                     'hourType' => {'name' => 'Admin'},
                     'project' => nil,
                     'hours' => 16,
                 },
                 {
                     'hourType' => {'name' => 'Task Time'},
                     'project' => {'name' => 'Project'},
                     'hours' => 16,
                 }
             )).to eq(1)
    end
  end
end
