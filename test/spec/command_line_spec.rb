require 'monoxide/invoice/cli'
require 'trollop'

describe Monoxide::Invoice::Cli do
  it 'accepts a :config option to determine the file to load' do
    config_file = 'test.config'

    File.should_receive(:exist?) do |f|
      expect(f).to eq(config_file)
      true
    end
    File.should_receive(:open) do |f, _ = 'r'|
      expect(f).to eq(config_file)

      <<-EOF
test:
  type: test
  name: Test client
  address: 42 Test St
      EOF
    end

    Monoxide::Invoice::Cli.new.parse_options({:config => config_file})
  end

  describe 'raises errors when' do
    before(:all) do
      # This is triggered as IntelliJ can't follow some of the custom stuff that Trollop uses instead of self.options
      #noinspection RubyArgCount
      Trollop::options({})
    end

    around(:each) do |block|
      silence { block.run }
    end

    it 'the configuration file doesn\'t exist' do
      expect {
        Monoxide::Invoice::Cli.new.run({:quiet => true, :config => '/non-existant-file'}, ['testing'])
      }.to raise_error SystemExit
    end

    it 'no template files have been passed to it' do
      expect {
        Monoxide::Invoice::Cli.new.run({:quiet => true}, [])
      }.to raise_error SystemExit
    end

    it 'a template file doesn\'t exist' do
      expect {
        Monoxide::Invoice::Cli.new.run({:quiet => true}, ['testing'])
      }.to raise_error SystemExit
    end
  end
end
