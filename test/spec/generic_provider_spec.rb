require 'monoxide/invoice/provider/generic_provider'

describe Monoxide::Invoice::Provider::GenericProvider do
  it 'throws an error on behalf of subclasses that don\'t implement #data' do
    expect {
      Monoxide::Invoice::Provider::GenericProvider.new.data
    }.to raise_error 'Unimplemented'
  end

  it 'throws an error if an invalid type of provider is attempted to be created' do
    expect {
      Monoxide::Invoice::Provider::GenericProvider.create_provider(:testing_non_existent, {})
    }.to raise_error 'Invalid type of timesheet provider: testing_non_existent'
  end
end
