require 'monoxide/invoice/provider/generic_provider'

describe Monoxide::Invoice::Provider::Harvest do
  it 'is returned when creating a provider with type :harvest' do
    #noinspection RubyStringKeysInHashInspection
    expect(Monoxide::Invoice::Provider::GenericProvider.create_provider(:harvest, {
        'auth' => {
            'username' => 'test',
            'password' => 'test',
            'subdomain' => 'test',
        }
    }).class).to eq(Monoxide::Invoice::Provider::Harvest)
  end

  describe '#data' do

  end
end
